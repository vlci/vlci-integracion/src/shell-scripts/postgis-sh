#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

# Global variables
LOGS_PATH=/var/log/etls/sc_vlci/shscripts/postgis-sh

function setArguments() {
    # Environment to import
    REFRESH_PRO=false
    REFRESH_PRE=false
    REFRESH_INT=false
    REFRESH_LOCAL=false

    case $1 in
    PRO)
        REFRESH_PRO=true
        ENV=PRO
        ;;
    PRE)
        REFRESH_PRE=true
        ENV=PRE
        ;;
    INT)
        REFRESH_INT=true
        ENV=INT
        ;;
    LOCAL)
        REFRESH_LOCAL=true
        ENV=LOCAL
        ;;
    *)
        echo "Uso: $ pg_refresh_views.sh (LOCAL|INT|PRE|PRO)"
        exit 2
        ;;
    esac
}

function setVariables() {
    SH_PATH=`readlink -f "${BASH_SOURCE:-$0}"`
    DEPLOY_SH_PATH=`dirname ${SH_PATH}`

    # Load Variables
    source ${DEPLOY_SH_PATH}/secrets.sh

    # Script Variables
    TIMESTAMP=$(date '+%Y%m%d%-H%M')
    DAY=$(date '+%Y%m%d')

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function setVariables: Variables loaded in the bash shell"
}

function refresh_views() {

    LIST_VIEWS=(vw_datos_cb_agua_total_materialized vw_datos_cb_residuos_materialized vw_datos_cb_emt_kpi_materialized vw_datos_cb_trafico_vehiculos_materialized vw_datos_cb_kpi_trafico_bicis_materialized)

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function refresh_views: Start refreshing views"

    for VIEW in "${LIST_VIEWS[@]}";
    do
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        PGPASSWORD=${VLCI_PASSWORD} psql -h ${POSTGIS_HOST} -p ${POSTGIS_PORT} -U ${VLCI_USER} -d ${VLCI_DB} -c "REFRESH MATERIALIZED VIEW vlci2.$VIEW;" > $LOGS_PATH/${TIMESTAMP}_${ENV}_pg_refresh_views.log 2>&1
        if [ $? -eq 0 ]; then
            LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
            echo "INFO - ${LOG_TIMESTAMP} - function refresh_views: ${VIEW} Refreshed successfully"
        fi
    done

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function refresh_views: Completed successfully"
}


############################################################
# MAIN PROGRAM
############################################################
if [ "$#" -ne 1 ]; then
    echo "Uso: $ pg_refresh_views.sh (LOCAL|INT|PRE|PRO)"
    exit 2
else
    setArguments $1
fi

setVariables

refresh_views

LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
if [ -s $LOGS_PATH/${TIMESTAMP}_pg_refresh_views.err ]; then
    # Error file with data, exit with error
    echo "ERROR - ${LOG_TIMESTAMP} - ERROR FOUND DURING THE PROCESS"
    RESULT=1
else
    # Error file without data
    echo "INFO - ${LOG_TIMESTAMP} - PROCESS COMPLETED SUCCESSFULLY"
fi

exit $RESULT
